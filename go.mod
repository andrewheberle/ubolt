module gitlab.com/andrewheberle/ubolt

go 1.16

require (
	github.com/stretchr/testify v1.7.0
	go.etcd.io/bbolt v1.3.6
)
